/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arifu.repositories;

import com.arifu.entities.StudentCourseMapping;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author samuel
 */
public interface StudentCourseMappingRepository extends JpaRepository<StudentCourseMapping, Integer> {
    
}
