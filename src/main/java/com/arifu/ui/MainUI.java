/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arifu.ui;

import com.arifu.entities.CertificateState;
import com.arifu.entities.Course;
import com.arifu.entities.Student;
import com.arifu.entities.StudentCertificateMapping;
import com.arifu.repositories.CertificateStateRepository;
import com.arifu.repositories.CourseRepository;
import com.arifu.repositories.StudentCertificateMappingRepository;
import com.arifu.repositories.StudentCourseMappingRepository;
import com.arifu.repositories.StudentRepository;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.PWA;
import java.util.Collection;
import org.claspina.confirmdialog.ButtonOption;
import org.claspina.confirmdialog.ConfirmDialog;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.crudui.crud.CrudListener;
import org.vaadin.crudui.crud.CrudOperation;
import org.vaadin.crudui.crud.impl.GridCrud;

/**
 *
 * @author samuel
 */
@Route
@PWA(name = "Project Base for Vaadin Flow with Spring", shortName = "Project Base")
public class MainUI extends VerticalLayout {

    @Autowired
    CertificateStateRepository certificateStateRepository;

    @Autowired
    CourseRepository courseRepository;

    @Autowired
    StudentCertificateMappingRepository studentCertificateMappingRepository;

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    StudentCourseMappingRepository studentCourseMappingRepository;

    VerticalLayout content = new VerticalLayout();
    VerticalLayout options = new VerticalLayout();

    public MainUI() {

        setSizeFull();

        SplitLayout layout = new SplitLayout();

        layout.setOrientation(SplitLayout.Orientation.HORIZONTAL);
        layout.setSplitterPosition(30);
        layout.setSizeFull();

        options.setSizeFull();

        options.add(
                createButton("Student", l -> {
                    GridCrud<Student> crud = new GridCrud<>(Student.class);
                    crud.getCrudFormFactory().setUseBeanValidation(true);
                    crud.setCrudListener(new CrudListener<Student>() {
                        @Override
                        public Collection<Student> findAll() {
                            return studentRepository.findAll();
                        }

                        @Override
                        public Student add(Student t) {
                            return studentRepository.save(t);
                        }

                        @Override
                        public Student update(Student t) {
                            return studentRepository.save(t);
                        }

                        @Override
                        public void delete(Student t) {
                            studentRepository.delete(t);
                        }
                    });

                    crud.setHeight("800px");
                    crud.setWidth("1000px");

                    ConfirmDialog
                            .createQuestion()
                            .withCaption("Student Data")
                            .withMessage(crud)
                            .withOkButton(() -> {
                                System.out.println("DONE");
                            }, ButtonOption.focus(), ButtonOption.caption("YES"))
                            .withCancelButton(ButtonOption.caption("NO"))
                            .open();
                }),
                createButton("Certificates", l -> {
                    GridCrud<StudentCertificateMapping> crud = new GridCrud<>(StudentCertificateMapping.class);
                    crud.getCrudFormFactory().setUseBeanValidation(true);
                    crud.setCrudListener(new CrudListener<StudentCertificateMapping>() {
                        @Override
                        public Collection<StudentCertificateMapping> findAll() {
                            return studentCertificateMappingRepository.findAll();
                        }

                        @Override
                        public StudentCertificateMapping add(StudentCertificateMapping t) {
                            return studentCertificateMappingRepository.save(t);
                        }

                        @Override
                        public StudentCertificateMapping update(StudentCertificateMapping t) {
                            return studentCertificateMappingRepository.save(t);
                        }

                        @Override
                        public void delete(StudentCertificateMapping t) {
                            studentCertificateMappingRepository.delete(t);
                        }
                    }
                    );

                    crud.getCrudFormFactory().setVisibleProperties(CrudOperation.ADD, "student", "certificateState");
                    crud.getCrudFormFactory().setFieldProvider("student", () -> {
                        ComboBox<Student> checkboxes = new ComboBox<>("Student", studentRepository.findAll());
                        return checkboxes;
                    });
                    crud.getCrudFormFactory().setFieldProvider("certificateState", () -> {
                        ComboBox<CertificateState> checkboxes = new ComboBox<>("certificateState", certificateStateRepository.findAll());
                        return checkboxes;
                    });

                    crud.setHeight("800px");
                    crud.setWidth("1000px");

                    ConfirmDialog
                            .createQuestion()
                            .withCaption("CERTIFICATE Data")
                            .withMessage(crud)
                            .withOkButton(() -> {
                                System.out.println("DONE");
                            }, ButtonOption.focus(), ButtonOption.caption("YES"))
                            .withCancelButton(ButtonOption.caption("NO"))
                            .open();
                }),
                createButton("Course", l -> {
                    GridCrud<Course> crud = new GridCrud<>(Course.class);
                    crud.getCrudFormFactory().setUseBeanValidation(true);
                    crud.setCrudListener(new CrudListener<Course>() {
                        @Override
                        public Collection<Course> findAll() {
                            return courseRepository.findAll();
                        }

                        @Override
                        public Course add(Course t) {
                            return courseRepository.save(t);
                        }

                        @Override
                        public Course update(Course t) {
                            return courseRepository.save(t);
                        }

                        @Override
                        public void delete(Course t) {
                            courseRepository.delete(t);
                        }
                    }
                    );

                    crud.setHeight("800px");
                    crud.setWidth("1000px");

                    ConfirmDialog
                            .createQuestion()
                            .withCaption("COURSE Data")
                            .withMessage(crud)
                            .withOkButton(() -> {
                                System.out.println("DONE");
                            }, ButtonOption.focus(), ButtonOption.caption("YES"))
                            .withCancelButton(ButtonOption.caption("NO"))
                            .open();
                })
        );
        content.setSizeFull();

        add(options);
    }

    Button createButton(String label, ComponentEventListener<ClickEvent<Button>> listener) {
        Button b = new Button(label);
        b.addClickListener(listener);
        return b;
    }

}
