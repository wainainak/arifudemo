package com.arifu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableAutoConfiguration
@EntityScan(basePackages = {"com.arifu.entities"})
@EnableJpaRepositories(basePackages = {"com.arifu.repositories"})
@SpringBootApplication
public class ArifuApplication  extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(ArifuApplication.class, args);
	}

}
