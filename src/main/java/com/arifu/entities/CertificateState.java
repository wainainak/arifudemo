/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arifu.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
//import lombok.Getter;
//import lombok.Setter;
//import lombok.experimental.Accessors;

/**
 *
 * @author samuel
 */
//@Accessors(fluent = true) @Getter @Setter
@Entity
@Table(name = "certificate_state")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CertificateState.findAll", query = "SELECT c FROM CertificateState c")
    , @NamedQuery(name = "CertificateState.findById", query = "SELECT c FROM CertificateState c WHERE c.id = :id")
    , @NamedQuery(name = "CertificateState.findByState", query = "SELECT c FROM CertificateState c WHERE c.state = :state")})
public class CertificateState implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "state")
    private String state;    

    public CertificateState() {
    }

    public CertificateState(Integer id) {
        this.id = id;
    }

    public CertificateState(Integer id, String state) {
        this.id = id;
        this.state = state;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CertificateState)) {
            return false;
        }
        CertificateState other = (CertificateState) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return state;
    }
    
}
